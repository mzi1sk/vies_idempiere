/**
 * CheckVatService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package eu.idempiere.vies;

public interface CheckVatService extends javax.xml.rpc.Service {
    public java.lang.String getcheckVatPortAddress();

    public eu.idempiere.vies.CheckVatPortType getcheckVatPort() throws javax.xml.rpc.ServiceException;

    public eu.idempiere.vies.CheckVatPortType getcheckVatPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
