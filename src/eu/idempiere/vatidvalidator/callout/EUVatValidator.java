package eu.idempiere.vatidvalidator.callout;

import java.rmi.RemoteException;
import java.util.Properties;

import org.adempiere.base.IColumnCallout;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MCountry;
import org.compiere.model.MLocation;
import org.compiere.util.CLogger;
import org.compiere.util.Env;

import eu.idempiere.vies.*;

import javax.xml.rpc.ServiceException;
import javax.xml.rpc.holders.*;

import org.apache.axis.holders.*;


public class EUVatValidator implements IColumnCallout {

    @Override
    public String start(Properties ctx, int WindowNo, GridTab mTab,
            GridField mField, Object value, Object oldValue) {
       
       
        if (value == null)
            return null;
        if (value.toString().length()<4)
        {
        	return "Not Valid VAT ID";
        }
        
      //  if ("Y".equals(Env.getContext(ctx, WindowNo, "_QUICK_ENTRY_MODE_"))) {
       //     if (mField.getColumnName().equalsIgnoreCase("Check VAT ID")) {
 
            	CheckVatPortType p;
				try {
					p = new CheckVatServiceLocator().getcheckVatPort();
			       StringHolder countryCode = new StringHolder();
                   StringHolder vatNumber= new StringHolder();
                   countryCode.value = value.toString().substring(0,2);
                   vatNumber.value = value.toString().substring(2,value.toString().length());
                   DateHolder requestDate = new DateHolder();
                   BooleanHolder valid = new BooleanHolder();
                   StringHolder name = new StringHolder();
                   StringHolder address= new StringHolder();
                   p.checkVat (countryCode,vatNumber,requestDate,valid,name,address);
 
                   if (!valid.value)
                   {
                   	return "Not Valid VAT ID";
                   }
		

			       System.out.println("countryCode:"+countryCode.value);
			       System.out.println("vatNumber:"+vatNumber.value);
			       System.out.println("requestDate:"+requestDate.value);
			       System.out.println("valid:"+valid.value);
			       System.out.println("name:"+name.value);
			       System.out.println("address:"+address.value);
			       
			       String[] laddress = ((String) address.value).split("\n");
	
			       mTab.setValue("Name", name.value);
			       
			       
			       
			       MLocation location=new MLocation(ctx, 0, null);
			       location.setAddress1(laddress[0]);
			       location.setPostal(laddress[1].substring(0,5));
			       location.setCity(laddress[1].substring(5,laddress[1].length()));
			       location.setC_Country_ID(MCountry.getAllIDs(MCountry.Table_Name, "countrycode='"+countryCode.value+"'", null)[0]);
			       location.saveEx();
			       
			       for (int i=0; i<mTab.getGridWindow().getTabCount(); i++) {
	                    GridTab tabaddress = mTab.getGridWindow().getTab(i);
	                    if (tabaddress.getAD_Table_ID() == MBPartnerLocation.Table_ID) {
	                    	tabaddress.setValue(MBPartnerLocation.COLUMNNAME_C_Location_ID, location.get_ID());
	                        break;
	                    }
	                }
	    		} catch (ServiceException e) {
	    	        	return "Vies Service Not Available ";
				} catch (RemoteException e) {
    	        	return "Vies Service Not Available ";
				}
      //      }
      //  }
        return null;
    }

}