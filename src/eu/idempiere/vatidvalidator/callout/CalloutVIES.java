/******************************************************************************
 * Product: Adempiere ERP & CRM Smart Business Solution                       *
 * Copyright (C) 1999-2006 ComPiere, Inc. All Rights Reserved.                *
 * This program is free software; you can redistribute it and/or modify it    *
 * under the terms version 2 of the GNU General Public License as published   *
 * by the Free Software Foundation. This program is distributed in the hope   *
 * that it will be useful, but WITHOUT ANY WARRANTY; without even the implied *
 * warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.           *
 * See the GNU General Public License for more details.                       *
 * You should have received a copy of the GNU General Public License along    *
 * with this program; if not, write to the Free Software Foundation, Inc.,    *
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA.                     *
 * For the text or an alternative of this public license, you may reach us    *
 * ComPiere, Inc., 2620 Augustine Dr. #245, Santa Clara, CA 95054, USA        *
 * or via info@compiere.org or http://www.compiere.org/license.html           *
 *****************************************************************************/
package eu.idempiere.vatidvalidator.callout;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;

import javax.xml.rpc.ServiceException;
import javax.xml.rpc.holders.BooleanHolder;
import javax.xml.rpc.holders.StringHolder;

import org.apache.axis.holders.DateHolder;
import org.compiere.model.CalloutEngine;
import org.compiere.model.GridField;
import org.compiere.model.GridTab;
import org.compiere.model.MBPartnerLocation;
import org.compiere.model.MCountry;
import org.compiere.model.MLocation;
import org.compiere.util.DB;
import org.compiere.util.Env;

import eu.idempiere.vies.CheckVatPortType;
import eu.idempiere.vies.CheckVatServiceLocator;


/**
 *	Resource Assignment Callout
 *	
 *  @author Jorg Janke
 *  @version $Id: CalloutAssignment.java,v 1.3 2006/07/30 00:51:03 jjanke Exp $
 */
public class CalloutVIES extends CalloutEngine
{

	/**
	 *	Assignment_Product.
	 *		- called from S_ResourceAssignment_ID
	 *		- sets M_Product_ID, Description
	 *			- Qty.. 
	 *	@param ctx context
	 *	@param WindowNo window no
	 *	@param mTab tab
	 *	@param mField field
	 *	@param value value
	 *	@return null or error message
	 */
	public String CheckVAT (Properties ctx, int WindowNo, GridTab mTab, GridField mField, Object value)
	{
		if (isCalloutActive() || value == null)
			return "";
	   	GridField ViesMessage = mTab.getField(2);
		//	get value
	       if (value == null)
	            return null;
	        if (value.toString().length()<4)
	        {
	        	ViesMessage.setErrorValue("Not Valid EU VAT TaxID");
	        	return "Not Valid VAT ID";
	        }
	        
	        if ("Y".equals(Env.getContext(ctx, WindowNo, "_QUICK_ENTRY_MODE_"))) {
	       //     if (mField.getColumnName().equalsIgnoreCase("Check VAT ID")) {
	 
	            	CheckVatPortType p;
					try {
						p = new CheckVatServiceLocator().getcheckVatPort();
				       StringHolder countryCode = new StringHolder();
	                   StringHolder vatNumber= new StringHolder();
	                   countryCode.value = value.toString().substring(0,2);
	                   vatNumber.value = value.toString().substring(2,value.toString().length());
	                   DateHolder requestDate = new DateHolder();
	                   BooleanHolder valid = new BooleanHolder();
	                   StringHolder name = new StringHolder();
	                   StringHolder address= new StringHolder();
	                   p.checkVat (countryCode,vatNumber,requestDate,valid,name,address);
	 
	                   if (!valid.value)
	                   {
	                   	ViesMessage.setError(true);
	                   	return "Not Valid VAT ID";
	                   }
			

				       System.out.println("countryCode:"+countryCode.value);
				       System.out.println("vatNumber:"+vatNumber.value);
				       System.out.println("requestDate:"+requestDate.value);
				       System.out.println("valid:"+valid.value);
				       System.out.println("name:"+name.value);
				       System.out.println("address:"+address.value);
				       
				       String[] laddress = ((String) address.value).split("\n");
		
				       mTab.setValue("Name", name.value);
				       
				       
				       
				       MLocation location=new MLocation(ctx, 0, null);
				       location.setAddress1(laddress[0]);
				       location.setPostal(laddress[1].substring(0,5));
				       location.setCity(laddress[1].substring(5,laddress[1].length()));
				       location.setC_Country_ID(MCountry.getAllIDs(MCountry.Table_Name, "countrycode='"+countryCode.value+"'", null)[0]);
				       location.saveEx();
				       
				       for (int i=0; i<mTab.getGridWindow().getTabCount(); i++) {
		                    GridTab tabaddress = mTab.getGridWindow().getTab(i);
		                    if (tabaddress.getAD_Table_ID() == MBPartnerLocation.Table_ID) {
		                    	tabaddress.setValue(MBPartnerLocation.COLUMNNAME_C_Location_ID, location.get_ID());
		                        break;
		                    }
		                }
		    		} catch (ServiceException e) {
		    	        	ViesMessage.setErrorValue("Vies Service Not Available ");
		    	        	return "Vies Service Not Available ";
					} catch (RemoteException e) {
	    	        	ViesMessage.setErrorValue("Vies Service Not Available ");
	    	        	return "Vies Service Not Available ";
					}
	            }
	      //  }
		return "";
	}	//	product

}	//	CalloutAssignment
