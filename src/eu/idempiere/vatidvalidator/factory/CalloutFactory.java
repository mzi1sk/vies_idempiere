package eu.idempiere.vatidvalidator.factory;

import java.util.ArrayList;
import java.util.List;

import org.adempiere.base.IColumnCallout;
import org.adempiere.base.IColumnCalloutFactory;
import org.compiere.model.MBPartner;

import eu.idempiere.vatidvalidator.callout.EUVatValidator;

public class CalloutFactory implements IColumnCalloutFactory {

	@Override
	public IColumnCallout[] getColumnCallouts(String tableName,
			String columnName) {
		
		List<IColumnCallout> list = new ArrayList<IColumnCallout>();
		
	//	if (tableName.equalsIgnoreCase(MBPartner.Table_Name)&& columnName.equalsIgnoreCase("checkVatID"))
	//		list.add(new EUVatValidator());
		if (tableName.equalsIgnoreCase(MBPartner.Table_Name)&& columnName.equalsIgnoreCase(MBPartner.COLUMNNAME_TaxID))
			list.add(new EUVatValidator());
		
		return list != null ? list.toArray(new IColumnCallout[0]) : new IColumnCallout[0];
	}

}
