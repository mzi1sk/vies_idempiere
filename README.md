IDempiere plugin for VAT Information Exchange System (VIES) 

It is an electronic means of transmitting information relating to VAT-registration (= validity of VAT-numbers) of companies registered in EU. Furthermore, information relating to (tax exempt) intra-Community supplies between Member States' administrations is also transmitted via VIES.